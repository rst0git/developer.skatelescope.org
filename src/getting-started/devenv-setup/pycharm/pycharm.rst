.. _pycharm:

PyCharm
*******

PyCharm is an alternative IDE for developing SKA control system software. :doc:`/getting-started/devenv-setup/vscode/vscode` is the recommended IDE.

Two versions of PyCharm are available: a free community edition, and a payware professional version. Both editions can
be downloaded from the `PyCharm download page`_. SKAO does not provide a professional license.

.. _`PyCharm download page`: https://www.jetbrains.com/pycharm/download

.. toctree::
  :maxdepth: 1
  :caption: Development tools
  :hidden:

  docker-configuration

- :doc:`docker-configuration`
